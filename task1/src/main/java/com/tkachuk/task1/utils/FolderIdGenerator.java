package com.tkachuk.task1.utils;

import java.util.concurrent.atomic.AtomicInteger;

public class FolderIdGenerator {

    private static final AtomicInteger id = new AtomicInteger(0);

    public static Long generateId() {
        return (long) id.incrementAndGet();
    }

}
