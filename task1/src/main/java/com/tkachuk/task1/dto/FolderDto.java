package com.tkachuk.task1.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Date;

import static java.lang.String.format;

@EqualsAndHashCode
@Getter
@RequiredArgsConstructor
public class FolderDto {

    private final Long id;
    private final String name;
    private final Date createAt;
    private final Date modifiedAt;

    @Override
    public String toString() {
        return format("Folder with id = [%s], name = [%s], created at [%s] and modified at [%s]",
                id, name, createAt, modifiedAt);
    }

}
