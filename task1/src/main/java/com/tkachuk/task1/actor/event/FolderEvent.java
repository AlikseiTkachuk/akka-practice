package com.tkachuk.task1.actor.event;

import com.tkachuk.task1.actor.CborSerializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


public class FolderEvent {

    public interface Event extends CborSerializable {
    }

    @NoArgsConstructor
    @AllArgsConstructor(staticName = "init")
    @Data
    public final static class FolderUpdated implements Event {
        private String newName;
    }

    @NoArgsConstructor
    @AllArgsConstructor(staticName = "init")
    @Data
    public final static class FolderCreated implements Event {
        private Long id;
        private String name;
    }

}
