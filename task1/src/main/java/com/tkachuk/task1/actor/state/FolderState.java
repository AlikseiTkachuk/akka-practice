package com.tkachuk.task1.actor.state;

import com.tkachuk.task1.actor.CborSerializable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

public class FolderState {

    public interface State extends CborSerializable {
    }

    public static class EmptyState implements State {
    }

    @Getter
    public static class ValidState implements State {
        private final Long id;
        @Setter
        private String name;
        private final Date createAt;
        @Setter
        private Date modifiedAt;

        public ValidState(Long id, String name) {
            this.id = id;
            this.name = name;
            this.createAt = new Date();
        }

        public Summary toSummary() {
            return new Summary(id, name, createAt, modifiedAt);
        }

        public ValidState update(final String newName) {
            this.name = newName;
            this.modifiedAt = new Date();
            return this;
        }

    }

    @RequiredArgsConstructor
    @ToString
    public static final class Summary {
        private final Long id;
        private final String name;
        private final Date createAt;
        private final Date modifiedAt;
    }

}
