package com.tkachuk.task1.actor;

import akka.actor.typed.Behavior;
import akka.actor.typed.SupervisorStrategy;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandler;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventHandlerBuilder;
import akka.persistence.typed.javadsl.EventSourcedBehavior;
import akka.persistence.typed.javadsl.RetentionCriteria;
import com.tkachuk.task1.actor.event.FolderEvent.FolderUpdated;
import com.tkachuk.task1.actor.state.FolderState.ValidState;
import com.tkachuk.task1.command.ConditionNotMetMessage;
import com.tkachuk.task1.command.SimpleDataMessage;
import lombok.Getter;

import java.time.Duration;

import static com.tkachuk.task1.actor.command.FolderCommand.Command;
import static com.tkachuk.task1.actor.command.FolderCommand.Create;
import static com.tkachuk.task1.actor.command.FolderCommand.Delete;
import static com.tkachuk.task1.actor.command.FolderCommand.Get;
import static com.tkachuk.task1.actor.command.FolderCommand.GetDataConditionally;
import static com.tkachuk.task1.actor.command.FolderCommand.Update;
import static com.tkachuk.task1.actor.event.FolderEvent.Event;
import static com.tkachuk.task1.actor.event.FolderEvent.FolderCreated;
import static com.tkachuk.task1.actor.state.FolderState.EmptyState;
import static com.tkachuk.task1.actor.state.FolderState.State;

@Getter
public class FolderActor extends EventSourcedBehavior<Command, Event, State> {

    public FolderActor(final Long id, final String name, ActorContext<Command> context) {
        super(PersistenceId.of("FolderActor", String.valueOf(id)),
                SupervisorStrategy.restartWithBackoff(Duration.ofMillis(200), Duration.ofSeconds(5), 0.1));
    }

    public static Behavior<Command> create(final Long id, final String folderName) {
        return Behaviors.setup(context -> new FolderActor(id, folderName, context));
    }

    @Override
    public State emptyState() {
        return new EmptyState();
    }

    @Override
    public CommandHandler<Command, Event, State> commandHandler() {
        var builder = newCommandHandlerBuilder();
        builder
                .forStateType(EmptyState.class)
                .onCommand(Create.class, (state, command) -> Effect().persist(FolderCreated.init(command.getId(), command.getName())));

        builder
                .forStateType(ValidState.class)
                .onCommand(Get.class, (state, command) -> {
                            command.getReplyTo().tell(SimpleDataMessage.of(state.toSummary()));
                            return Effect().none();
                        }
                )
                .onCommand(GetDataConditionally.class, (state, command) -> {
                            if (state.getName().equals(command.getNameCondition())) {
                                command.getReplyTo().tell(SimpleDataMessage.of(state.toSummary()));
                            } else {
                                command.getReplyTo().tell(ConditionNotMetMessage.of(command.getNameCondition()));
                            }
                            return Effect().none();
                        }
                )
                .onCommand(Update.class, (command) -> Effect().persist(FolderUpdated.init(command.getNewName())))
                .onCommand(Delete.class, (command) -> Effect().stop())
                .onAnyCommand(command -> Effect().none());
        return builder.build();
    }


    @Override
    public EventHandler<State, Event> eventHandler() {
        EventHandlerBuilder<State, Event> builder = newEventHandlerBuilder();
        builder.forStateType(EmptyState.class)
                .onEvent(FolderCreated.class, (state, created) -> new ValidState(created.getId(), created.getName()));

        builder.forStateType(ValidState.class)
                .onEvent(FolderUpdated.class, (state, updated) -> state.update(updated.getNewName()))
                .build();
        return builder.build();
    }

    @Override
    public RetentionCriteria retentionCriteria() {
        return RetentionCriteria.snapshotEvery(100, 2);
    }

}
