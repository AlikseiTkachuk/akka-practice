package com.tkachuk.task1.actor;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.tkachuk.task1.command.DataMessage;
import org.slf4j.Logger;

import java.util.Collection;
import java.util.Collections;

public class PrintActor extends AbstractBehavior<DataMessage> {

    public PrintActor(ActorContext<DataMessage> context) {
        super(context);
    }

    public static Behavior<DataMessage> create() {
        return Behaviors.setup(PrintActor::new);
    }

    @Override
    public Receive<DataMessage> createReceive() {
        return newReceiveBuilder()
                .onAnyMessage(message -> {
                    Object data = message.getData();
                    if (data instanceof Collection) {
                        return logCollection((Collection) data);
                    } else {
                        return log(data);
                    }
                })
                .build();
    }

    public Behavior<DataMessage> log(Object data) {
        getContext().getLog().info("Retrieved DataMessage - {}", data);
        return this;
    }

    public Behavior<DataMessage> logCollection(Collection collection) {
        var log = getContext().getLog();
        log.info("Retrieved DataMessage -  collection with [{}] elements", collection.size());
        collection.forEach(item -> log.info("Item = {}", item));
        return this;
    }
}
