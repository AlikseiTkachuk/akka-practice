package com.tkachuk.task1.actor.state;

import akka.actor.typed.ActorRef;
import com.tkachuk.task1.actor.CborSerializable;
import com.tkachuk.task1.actor.command.FolderCommand;
import lombok.ToString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ManagerState {

    public interface State extends CborSerializable {
    }

    public static class Empty implements State {
    }

    public static class NotBlank implements State {
        private final Map<Long, ActorRef<FolderCommand.Command>> folders = new HashMap<>();

        public ActorRef<FolderCommand.Command> getFolderActor(final Long id) {
            return folders.get(id);
        }

        public boolean isFolderExist(final Long id) {
            return folders.containsKey(id);
        }

        public ActorRef<FolderCommand.Command> addFolderActor(final Long id, final ActorRef<FolderCommand.Command> folderActor) {
            return folders.put(id, folderActor);
        }

        public ActorRef<FolderCommand.Command> removeFolderActor(final Long id) {
            return folders.remove(id);
        }

        public List<ActorRef<FolderCommand.Command>> folderActors() {
            return new ArrayList<>(folders.values());
        }

    }

}
