package com.tkachuk.task1.actor.command;

import akka.actor.typed.ActorRef;
import com.tkachuk.task1.actor.CborSerializable;
import com.tkachuk.task1.command.DataMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

public class FolderCommand {

    public interface Command extends CborSerializable {
    }

    @NoArgsConstructor(staticName = "init")
    public final static class Delete implements Command {
    }

    @RequiredArgsConstructor(staticName = "init")
    public final static class Get implements Command {
        @Getter
        private final ActorRef<DataMessage> replyTo;
    }

    @RequiredArgsConstructor(staticName = "init")
    public final static class Update implements Command {
        @Getter
        private final String newName;
    }

    @RequiredArgsConstructor(staticName = "init")
    @Getter
    public final static class Create implements Command {
        private final Long id;
        private final String name;
    }

    @RequiredArgsConstructor(staticName = "init")
    @Getter
    public final static class GetDataConditionally implements Command {
        private final ActorRef<DataMessage> replyTo;
        private final String nameCondition;
    }


}
