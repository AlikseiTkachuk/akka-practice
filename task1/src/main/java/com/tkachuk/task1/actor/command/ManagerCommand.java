package com.tkachuk.task1.actor.command;

import akka.actor.typed.ActorRef;
import com.tkachuk.task1.actor.CborSerializable;
import com.tkachuk.task1.command.DataMessage;
import lombok.Data;
import lombok.RequiredArgsConstructor;

public class ManagerCommand {

    public interface Command extends CborSerializable { }

    public static CreateFolder createFolder(final String name) {
        return new CreateFolder(name);
    }

    public static DeleteFolder deleteFolder(final Long id) {
        return new DeleteFolder(id);
    }

    public static GetFolder getFolder(final Long id, final ActorRef<DataMessage> replyTo) {
        return new GetFolder(id, replyTo);
    }

    public static GetAllFolders getAllFolders(final ActorRef<DataMessage> replyTo) {
        return new GetAllFolders(replyTo);
    }

    public static UpdateFolder updateFolder(final Long id, final String newName) {
        return new UpdateFolder(id, newName);
    }

    public static GetAllFoldersConditionally getAllFoldersConditionally(final String condition, final ActorRef<DataMessage> replyTo) {
        return new GetAllFoldersConditionally(condition, replyTo);
    }


    @Data
    @RequiredArgsConstructor
    public final static class CreateFolder implements Command {
        private final String name;
    }

    @Data
    @RequiredArgsConstructor
    public final static class DeleteFolder implements Command {
        private final Long id;
    }

    @Data
    @RequiredArgsConstructor
    public final static class GetFolder implements Command {
        private final Long id;
        private final ActorRef<DataMessage> replyTo;
    }

    @Data
    @RequiredArgsConstructor
    public final static class UpdateFolder implements Command {
        private final Long id;
        private final String newName;
    }

    @Data
    @RequiredArgsConstructor
    public final static class GetAllFolders implements Command {
        private final ActorRef<DataMessage> replyTo;
    }

    @Data
    @RequiredArgsConstructor
    public final static class GetAllFoldersConditionally implements Command {
        private final String nameCondition;
        private final ActorRef<DataMessage> replyTo;
    }

}
