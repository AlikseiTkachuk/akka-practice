package com.tkachuk.task1.actor;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.tkachuk.task1.actor.command.FolderCommand;
import com.tkachuk.task1.command.DataMessage;
import com.tkachuk.task1.command.SimpleDataMessage;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.tkachuk.task1.actor.command.FolderCommand.Get;
import static com.tkachuk.task1.actor.command.FolderCommand.GetDataConditionally;

public class FolderCollectorActor extends AbstractBehavior<FolderCollectorActor.Command> {

    private final ActorRef<DataMessage> replyTo;
    private final List<DataMessage> replies = new ArrayList<>();
    private final Collection<ActorRef<FolderCommand.Command>> folderActors;
    @Setter
    private int timeOut = 10;

    private FolderCollectorActor(Collection<ActorRef<FolderCommand.Command>> folderActors,
                                 ActorContext<Command> context,
                                 ActorRef<DataMessage> replyTo,
                                 String condition) {
        super(context);
        this.replyTo = replyTo;
        this.folderActors = folderActors;
        context.setReceiveTimeout(Duration.ofSeconds(timeOut), ReceiveTimeOut.INSTANCE);
        ActorRef<DataMessage> adaptedActorRef = context.messageAdapter(DataMessage.class, SingleReply::new);
        Optional.ofNullable(condition)
                .ifPresentOrElse(conditionValue ->
                        {
                            if (folderActors.isEmpty()) {
                                replyTo.tell(SimpleDataMessage.of("You passed to Folder Collector Actor empty folder collection"));
                            } else {
                                this.folderActors.forEach(folderActor -> folderActor.tell(GetDataConditionally.init(adaptedActorRef, conditionValue)));
                            }
                        },
                        () -> {
                            if (folderActors.isEmpty()) {
                                replyTo.tell(SimpleDataMessage.of("You passed to Folder Collector Actor empty folder collection"));
                            } else {
                                this.folderActors.forEach(folderActor -> folderActor.tell(Get.init(adaptedActorRef)));
                            }
                        }
                );
    }

    public static Behavior<Command> create(Collection<ActorRef<FolderCommand.Command>> folderActors,
                                           ActorRef<DataMessage> replyTo) {
        return Behaviors.setup(context -> new FolderCollectorActor(folderActors, context, replyTo, null));
    }

    public static Behavior<Command> create(Collection<ActorRef<FolderCommand.Command>> folderActors,
                                           ActorRef<DataMessage> replyTo, String condition) {
        return Behaviors.setup(context -> new FolderCollectorActor(folderActors, context, replyTo, condition));
    }

    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder()
                .onMessage(ReceiveTimeOut.class, this::onTimeOut)
                .onMessage(SingleReply.class, this::onReply)
                .build();
    }

    public interface Command extends CborSerializable {
    }

    public enum ReceiveTimeOut implements Command {
        INSTANCE;
    }

    @RequiredArgsConstructor
    @Data
    public class SingleReply implements Command {
        private final DataMessage dataMessage;
    }

    private Behavior<Command> onReply(SingleReply reply) {
        replies.add(reply.getDataMessage());
        if (folderActors.size() == replies.size()) {
            replyTo.tell(SimpleDataMessage.of(replies.stream().map(DataMessage::getData).collect(Collectors.toList())));
            return Behaviors.stopped();
        } else {
            return this;
        }
    }

    private Behavior<Command> onTimeOut(ReceiveTimeOut timeOut) {
        return Behaviors.stopped();
    }

}
