package com.tkachuk.task1.actor.event;

import com.tkachuk.task1.actor.CborSerializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class ManagerEvent {

    public interface Event extends CborSerializable {}

    @NoArgsConstructor
    @AllArgsConstructor(staticName = "init")
    @Data
    public static class FolderDeleted implements Event {
        private Long id;
    }

    @NoArgsConstructor
    @AllArgsConstructor(staticName = "init")
    @Data
    public static class FolderCreated implements Event {
        private String name;
    }

}
