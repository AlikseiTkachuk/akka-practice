package com.tkachuk.task1.actor;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandler;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehavior;
import akka.persistence.typed.javadsl.RetentionCriteria;
import com.tkachuk.task1.actor.command.FolderCommand;
import com.tkachuk.task1.actor.command.ManagerCommand;
import com.tkachuk.task1.actor.event.ManagerEvent.FolderCreated;
import com.tkachuk.task1.actor.state.ManagerState;
import com.tkachuk.task1.utils.FolderIdGenerator;

import static com.tkachuk.task1.actor.command.FolderCommand.Create;
import static com.tkachuk.task1.actor.command.FolderCommand.Delete;
import static com.tkachuk.task1.actor.command.FolderCommand.Get;
import static com.tkachuk.task1.actor.command.FolderCommand.Update;
import static com.tkachuk.task1.actor.command.ManagerCommand.Command;
import static com.tkachuk.task1.actor.command.ManagerCommand.CreateFolder;
import static com.tkachuk.task1.actor.command.ManagerCommand.DeleteFolder;
import static com.tkachuk.task1.actor.command.ManagerCommand.GetAllFolders;
import static com.tkachuk.task1.actor.command.ManagerCommand.GetAllFoldersConditionally;
import static com.tkachuk.task1.actor.command.ManagerCommand.GetFolder;
import static com.tkachuk.task1.actor.command.ManagerCommand.UpdateFolder;
import static com.tkachuk.task1.actor.event.ManagerEvent.Event;
import static com.tkachuk.task1.actor.event.ManagerEvent.FolderDeleted;
import static com.tkachuk.task1.actor.state.ManagerState.Empty;
import static com.tkachuk.task1.actor.state.ManagerState.NotBlank;
import static com.tkachuk.task1.actor.state.ManagerState.State;


public class FoldersManagerActor extends EventSourcedBehavior<Command, Event, State> {

    private final ActorContext<Command> context;

    public FoldersManagerActor(final String id, ActorContext<Command> context) {
        super(PersistenceId.of("FolderActor", id));
        this.context = context;
    }

    public static Behavior<Command> create() {
        return Behaviors.setup(context -> new FoldersManagerActor("333", context));
    }

    @Override
    public State emptyState() {
        return new ManagerState.Empty();
    }

    @Override
    public CommandHandler<Command, Event, State> commandHandler() {
        var commandHandler = newCommandHandlerBuilder();
        commandHandler
                .forStateType(Empty.class)
                .onCommand(CreateFolder.class, (event, command) -> Effect().persist(FolderCreated.init(command.getName())))
//                .onCommand(ManagerCommand.GetAllFolders.class, (event, command) -> Effect().stash())
//                .onCommand(ManagerCommand.GetAllFoldersConditionally.class, (event, command) -> Effect().stash())
//                .onCommand(ManagerCommand.GetFolder.class, (event, command) -> Effect().stash())
                .onAnyCommand(command -> Effect().none());

        commandHandler
                .forStateType(NotBlank.class)
                .onCommand(DeleteFolder.class, (state, command) ->
                        state.isFolderExist(command.getId())
                                ? Effect().persist(FolderDeleted.init(command.getId()))
                                : Effect().none()
                )
                .onCommand(CreateFolder.class, (state, command) -> Effect().persist(FolderCreated.init(command.getName())))
                .onCommand(UpdateFolder.class, (state, command) -> {
                            if (state.isFolderExist(command.getId())) {
                                state.getFolderActor(command.getId()).tell(Update.init(command.getNewName()));
                            }
                            return Effect().none();
                        }
                )
                .onCommand(GetFolder.class, (state, command) -> {
                    if (state.isFolderExist(command.getId())) {
                        ActorRef<FolderCommand.Command> folderActor = state.getFolderActor(command.getId());
                        folderActor.tell(Get.init(command.getReplyTo()));
                    }
                    return Effect().none();
                })
                .onCommand(GetAllFolders.class, (state, command) -> {
                    context.spawnAnonymous(FolderCollectorActor.create(state.folderActors(), command.getReplyTo()));
                    return Effect().none();
                })
                .onCommand(GetAllFoldersConditionally.class, (state, command) -> {
                    context.spawnAnonymous(FolderCollectorActor.create(state.folderActors(), command.getReplyTo(), command.getNameCondition()));
                    return Effect().none();
                })
                .onAnyCommand(command -> Effect().none());
        return commandHandler.build();
    }

    @Override
    public EventHandler<State, Event> eventHandler() {
        var eventHandler = newEventHandlerBuilder();

        eventHandler.forStateType(Empty.class)
                .onEvent(FolderCreated.class, (state, command) -> {
                    var name = command.getName();
                    var id = FolderIdGenerator.generateId();
                    var newState = new NotBlank();
                    newState.addFolderActor(id, spawnFolder(id, name));
                    return newState;
                });

        eventHandler.forStateType(NotBlank.class)
                .onEvent(FolderCreated.class, (state, command) -> {
                    var name = command.getName();
                    var id = FolderIdGenerator.generateId();
                    state.addFolderActor(id, spawnFolder(id, name));
                    return state;
                })
                .onEvent(FolderDeleted.class, (state, command) -> {
                    var id = command.getId();
                    state.getFolderActor(id).tell(Delete.init());
                    state.removeFolderActor(id);
                    return state;
                });
        return eventHandler.build();
    }

    private ActorRef<FolderCommand.Command> spawnFolder(Long id, String name) {
        ActorRef<FolderCommand.Command> actor = context.spawn(FolderActor.create(id, name), name + "_" + id);
        actor.tell(Create.init(id, name));
        return actor;
    }

    @Override
    public RetentionCriteria retentionCriteria() {
        return RetentionCriteria.snapshotEvery(100, 2);
    }


}
