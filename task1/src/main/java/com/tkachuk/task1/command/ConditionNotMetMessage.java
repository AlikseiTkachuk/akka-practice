package com.tkachuk.task1.command;

import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor(staticName = "of")
@ToString
public class ConditionNotMetMessage implements DataMessage<String> {

    private final String WARNING = "Condition [%s] not met";

    private final String condition;

    @Override
    public String getData() {
        return String.format(WARNING, condition);
    }
}
