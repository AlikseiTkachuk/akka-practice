package com.tkachuk.task1.command;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class SimpleDataMessage<T> implements DataMessage<T> {

    private final T data;

    private SimpleDataMessage(T data) {
        this.data = data;
    }

    public static <T> SimpleDataMessage<T> of(T data) {
        return new SimpleDataMessage<>(data);
    }

    @Override
    public T getData() {
        return data;
    }

}
