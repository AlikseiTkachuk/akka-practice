package com.tkachuk.task1.command;

public interface DataMessage<T> extends PrinterMessage {

    T getData();

}
