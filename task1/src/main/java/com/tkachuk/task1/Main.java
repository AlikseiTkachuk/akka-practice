package com.tkachuk.task1;

import akka.actor.typed.ActorSystem;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.Behaviors;
import com.tkachuk.task1.actor.FoldersManagerActor;
import com.tkachuk.task1.actor.PrintActor;
import com.tkachuk.task1.actor.command.ManagerCommand;

public class Main {

    public static void main(String[] args) {
        ActorSystem<Object> folderAkkaSystem = ActorSystem.create(rootBehavior(), "folderAkkaSystem1");
        folderAkkaSystem.tell("go");
    }


    public static Behavior<Object> rootBehavior() {
        return Behaviors.setup(context -> {

            var manager = context.spawn(FoldersManagerActor.create(), "folderManager");
            var printer = context.spawn(PrintActor.create(), "printActor");

//            manager.tell(ManagerCommand.createFolder("DOCUMENTS"));
//            manager.tell(ManagerCommand.createFolder("WORK"));
//            manager.tell(ManagerCommand.createFolder("DOWNLOADS"));
//            manager.tell(ManagerCommand.createFolder("PROJECTS"));
            manager.tell(ManagerCommand.deleteFolder(1L));
            manager.tell(ManagerCommand.getAllFolders(printer));
            manager.tell(ManagerCommand.getFolder(1L, printer));
//            manager.tell(ManagerCommand.updateFolder(2L, "NEW WORK"));
//            manager.tell(ManagerCommand.getAllFoldersConditionally("DOWNLOADS", printer));
            manager.tell(ManagerCommand.getAllFolders(printer));
            return Behaviors.same();
        });
    }
}
